package com.bettercoding.vbb.client.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RestClient {
    private static final RestClient ourInstance = new RestClient();
    public static final String BASE_URL = "http://192.168.1.175:8080";
    private Retrofit retrofit;
    private VbbServerService vbbServerService;

    private RestClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        vbbServerService = retrofit.create(VbbServerService.class);
    }

    public VbbServerService getVbbServerService() {
        return vbbServerService;
    }

    public static RestClient getInstance() {
        return ourInstance;
    }
}
