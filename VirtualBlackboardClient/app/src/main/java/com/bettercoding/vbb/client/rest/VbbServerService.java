package com.bettercoding.vbb.client.rest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface VbbServerService {
    @POST("/channel/{channelId}")
    @Headers("Content-Type:application/octet-stream")
    Call<Void> sendData(@Path("channelId") String channelId, @Body String data);

    @DELETE("/channel/{channelId}")
    Call<Void> clearChannel(@Path("channelId") String channelId);
}
