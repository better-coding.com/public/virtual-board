package com.bettercoding.vbb.client;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bettercoding.vbb.client.rest.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private final String sampleImage = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4wMREy0uEZ3DrwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAACtSURBVFjD7VY7FsAgDJK83v/KdnVQ8yO2g4ytkh+irV18DMw+9tb6sAAR4pFjxwNtYySJGceKR9gtXQVfQZik3uCuDmjk2n+zBjQyGHVj1ZBUz1gTMLIt9XbLnEBUVN6jiwple3wD7Ll7TQtM8UVsW6oqO25E1NvwpA5QWX3aB1it3yUSNiKLE1qSkGzlSI4RkeAg7nvINxu8upGisw36g6TKCU3vgargFxe/wAuB2T4gUCclbQAAAABJRU5ErkJggg==";
    private final String channelId = "example";

    private Button sendButton;
    private Button clearButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sendButton = findViewById(R.id.sendButton);
        clearButton = findViewById(R.id.clearButton);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestClient.getInstance().getVbbServerService().sendData(channelId, sampleImage).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(MainActivity.this, "Data sent", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Log.d("lc", "Service call failed", t);
                        Toast.makeText(MainActivity.this, "Call failed", Toast.LENGTH_SHORT).show();
                    }
                });

                Log.d("lc", "Data was sent to the server");
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestClient.getInstance().getVbbServerService().clearChannel(channelId).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(MainActivity.this, "Channel cleared", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Log.d("lc", "Service call failed", t);
                        Toast.makeText(MainActivity.this, "Call failed", Toast.LENGTH_SHORT).show();
                    }
                });

                Log.d("lc", "Data was sent to the server");
            }
        });
    }
}
