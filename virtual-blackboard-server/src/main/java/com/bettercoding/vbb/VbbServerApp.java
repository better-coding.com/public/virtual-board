package com.bettercoding.vbb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VbbServerApp {
    public static void main(String[] args) {
        SpringApplication.run(VbbServerApp.class, args);
    }
}
