package com.bettercoding.vbb;

import com.bettercoding.vbb.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class StreamController {

    private final Utils utils;

    private static final String KEYWORD_BASE64_DATA = "<BASE64_DATA>";
    private static final String KEYWORD_IMAGE_DATA = "<IMAGE_DATA>";
    private static final String TMPLT_SRC_CONTENT_WITH_BASE64 = "data:image/png;base64," + KEYWORD_BASE64_DATA;

    private String imgMissingStream;
    private String channelTemplate;

    private final Map<String, String> channels = new LinkedHashMap<>();

    @Autowired
    public StreamController(Utils utils) {
        this.utils = utils;
    }

    @PostConstruct
    public void init() {
        imgMissingStream = utils.readResourceAsBase64("images/missing-stream.png");
        channelTemplate = utils.readResourceAsString("html/channel.html");
    }

    @GetMapping("/")
    public String welcomePage() {
        return "Go to /channel/{channelId}";
    }

    @GetMapping("/channel/{channelId}")
    public String readChannel(@PathVariable("channelId") String channelId) {
        String imageBase64 = channels.getOrDefault(channelId, imgMissingStream);
        String srcContent = TMPLT_SRC_CONTENT_WITH_BASE64.replace(KEYWORD_BASE64_DATA, imageBase64);
        return channelTemplate.replace(KEYWORD_IMAGE_DATA, srcContent);
    }

    @PostMapping("/channel/{channelId}")
    public void writeChannel(@PathVariable("channelId") String channelId, @RequestBody String data) {
        channels.put(channelId, data);
    }

    @DeleteMapping("/channel/{channelId}")
    public void deleteChannel(@PathVariable("channelId") String channelId) {
        channels.remove(channelId);
    }
}
