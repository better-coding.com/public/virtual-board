package com.bettercoding.vbb.utils;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

@Component
public class Utils {

    private static final String UTF_8 = "UTF-8";

    public String readResourceAsString(String name) {
        try {
            return new String(readResource(name), UTF_8);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Unsupported encoding exception occurred", e);
        }
    }

    public byte[] readResource(String name) {
        try {
            return Files.readAllBytes(Paths.get(Utils.class.getClassLoader().getResource(name).toURI()));
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("Exception occurred during loading missing stream image", e);
        }
    }

    public String toBase64(byte[] data) {
        try {
            return new String(Base64.getEncoder().encode(data), UTF_8);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Unsupported encoding exception occurred", e);
        }
    }

    public String readResourceAsBase64(String name) {
        return toBase64(readResource(name));
    }
}
